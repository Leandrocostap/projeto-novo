import React from 'react'
import styles from './Home.module.css'

function Home (){

    const [artigoName, setArtigoName] = React.useState('')
    const [artigo, setArtigo] = React.useState('')
    const [artigos, setArtigos] = React.useState([])

    //accordion
    const [accordionOne, setAccordionOne] = React.useState(false)
    const [accordionTwo, setAccordionTwo] = React.useState(false)
    const [accordionThree, setAccordionThree] = React.useState(false)
    const [accordionFour, setAccordionFour] = React.useState(false)
    const [accordionFive, setAccordionFive] = React.useState(false)




    return(

    <div className={styles.Principal}>
         <nav classNameName={styles.nav1}>
             
            <a href="https://www.linkedin.com/in/leandrocostap/">Contato</a>
            <a href="#">Sobre</a>
            <a href="#">Inicio</a>
            <h5 className={styles.textoh5}>BLOG<span>FRONT</span></h5>
        </nav>

        <img className={styles.fotologo} src="123.jpg" width="100%" height="300px" alt=""></img>
        
        
                <h5 className={styles.h5}>Para mais informações</h5>
        <div className={styles.centertext}>
                <input type="text" name id placeholder="Digite aqui seu email"></input>
                <button>Enviar</button>
        </div>


        <h1>PARA SE TORNAR UM DESENVOLVEDOR FRONT-END ↓</h1>

        <p className={styles.paragrafo1}>O desenvolvedor front-end é responsável pela experiência do usuário dentro de uma aplicação web, é ele quem vai desenhar e desenvolver as páginas com as quais, posteriormente, o usuário irá interagir.
            O front-end também inclui elementos que determinam a identidade visual de um site ou aplicativo, por isso, além do conhecimento de linguagens de programação específicas, um desenvolvedor dessa área só tem a ganhar se tiver noções de design, arquitetura da informação e UX.
            Para ser um desenvolvedor front-end você precisa se familiarizar com lógica de programação aprender algumas linguagens, como HTML, CSS, Javascript e Flash. Essas são as principais linguagens utilizadas para criar interfaces de uma aplicação.</p>

            <img className={styles.imagemmeio1} src="pngwing.com (5).png" height="150px" alt=""></img>
            <img className={styles.imagemmeio2} src="pngwing.com (3).png" height="150px" alt=""></img>
        
        <div className={styles.accordion_div}>

            <button onClick={() => {setAccordionOne(!accordionOne)}} className={styles.accordion}>Lógica de Programação</button>
            <div className={`${ styles.panel  } ${accordionOne ? styles.show:styles.none}` }>
                <p className={styles.paragrafo2}>Lógica de programação é a organização coesa de uma sequência de instruções voltadas à resolução de um problema, ou à criação de um software ou aplicação.
                    A lógica de programação é o conhecimento anterior a qualquer outro quando falamos em desenvolvimento web porque é a partir dele que os aprendizados posteriores, como por exemplo o das linguagens de programação, fará sentido.
                    Cada linguagem tem suas próprias particularidades, como sua sintaxe, seus tipos de dados e sua orientação, mas a lógica por trás de todas é a mesma.</p>
            </div>

            <button onClick={() => {setAccordionTwo(!accordionTwo)}} className={styles.accordion}>HTML 5</button>
            <div className={`${ styles.panel  } ${accordionTwo ? styles.show:styles.none}` }>
                <p className={styles.paragrafo2}>Criada pelo britânico Tim Berners-Lee, o acrônimo HTML significa HiperText Markup Language, traduzindo ao português: Linguagem de Marcação de Hipertexto. O HTML é o componente básico da web, ele permite inserir o conteúdo e estabelecer a estrutura básica de um website. Portanto, ele serve para dar significado e organizar as informações de uma página na web. Sem isso, o navegador não saberia exibir textos como elementos ou carregar imagens e outros conteúdos.
                    Os hipertextos são conjuntos de elementos conectados. Esses podem ser palavras, imagens, vídeos, documento, etc. Quando conectados, formam uma rede de informações que permite a comunicação de dados, organizando conhecimentos e guardando informações.
                    Ao visitar uma página simples na web, você pode perceber que existem diferentes distribuições e tamanhos para títulos, parágrafos, imagens, vídeos e qualquer outro elemento. Essa estrutura é estabelecida através do HTML. No inicio da web, era comum encontrar sites apenas contendo textos e imagens simples, com estrutura básica e sem estilizações. Porém, nos dias atuais, muito dificilmente você encontrará sites que possuam apenas elementos HTML. Portanto, podemos considerar o HTML o “esqueleto” da sua página</p>
            </div>

            <button onClick={() => {setAccordionThree(!accordionThree)}} className={styles.accordion}>CSS</button>
            <div  className={`${ styles.panel  } ${accordionThree ? styles.show:styles.none}` }>
                <p className={styles.paragrafo2}>CSS é chamado de linguagem Cascading Style Sheet e é usado para estilizar elementos escritos em uma linguagem de marcação como HTML. O CSS separa o conteúdo da representação visual do site. Pense na decoração da sua página. Utilizando o CSS é possível alterar a cor do texto e do fundo, fonte e espaçamento entre parágrafos. Também pode criar tabelas, usar variações de layouts, ajustar imagens para suas respectivas telas e assim por diante.
                    CSS foi desenvolvido pelo W3C (World Wide Web Consortium) em 1996, por uma razão bem simples. O HTML não foi projetado para ter tags que ajudariam a formatar a página. Você deveria apenas escrever a marcação para o site.
                    Tags como foram introduzidas na versão 3.2 do HTML e causaram muitos problemas para os desenvolvedores. Como os sites tinham diferentes fontes, cores e estilos, era um processo longo, doloroso e caro para reescrever o código. Assim, o CSS foi criado pelo W3C para resolver este problema.
                    A relação entre HTML e CSS é bem forte. Como o HTML é uma linguagem de marcação (o alicerce de um site) e o CSS é focado no estilo (toda a estética de um site), eles andam juntos.
                    CSS não é tecnicamente uma necessidade, mas provavelmente você não gostaria de olhar para um site que usa apenas HTML, pois isso pareceria completamente abandonado.</p>
            </div>

            <button onClick={() => {setAccordionFour(!accordionFour)}} className={styles.accordion}>Desing Responsivo</button>
            <div className={`${ styles.panel  } ${accordionFour ? styles.show:styles.none}` }>
                <p className={styles.paragrafo2}>Design Responsivo é a ação de fazer com que uma página na internet possa ser acessada por qualquer tipo de aparelho ou máquina, independente da sua resolução de tela. Isso inclui computadores, notebooks, tablets, smartphones e outros dispositivos portáteis.
                    Esta característica é alcançada com a combinação de duas linguagens de programação bastante populares: o HTML, responsável pela estrutura textual, e o CSS, que compõe e agrupa todo o estilo visual de um site.
                    O objetivo principal de um site com design responsivo é fazer com que ele possa ser acessado por um maior número de pessoas possível. Quanto mais aparelhos, telas e resoluções ele for compatível, maior serão as chances de conseguir atrair grandes quantidades de tráfego.
                    Além disso, o conceito de Design Responsivo pode também ser estendido à Experiência do Usuário. Pouco adianta um site ser responsivo se ele não entrega uma alta qualidade de usabilidade, considerando a coerência no posicionamento dos elementos junto com o desempenho. Seja em qualquer tipo de aparelho em que for acessado.</p>
            </div>

            <button onClick={() => {setAccordionFive(!accordionFive)}} className={styles.accordion}>Java Script</button>
            <div className={`${ styles.panel  } ${accordionFive ? styles.show:styles.none}` }>
                <p className={styles.paragrafo2}>JavaScript é uma linguagem de programação criada em 1995 por Brendan Eich enquanto trabalhava na Netscape Communications Corporation. Originalmente projetada para rodar no Netscape Navigator, ela tinha o propósito de oferecer aos desenvolvedores formas de tornar determinados processos de páginas web mais dinâmicos, tornando seu uso mais agradável. Um ano depois de seu lançamento, a Microsoft portou a linguagem para seu navegador, o que ajudou a consolidar a linguagem e torná-la uma das tecnologias mais importantes e utilizadas na internet
                    Embora ela tenha esse nome, não se deve confundir JavaScript com Java, linguagem de programação desenvolvida pela Sun Microsystems: antes, a linguagem criada pela Netscape recebera nomes como LiveScript e Mocha, mas, para aproveitar o grande sucesso da linguagem da Sun no mercado, os executivos da Netscape resolveram mudar o nome de sua linguagem para o atual. Entretanto, Java e Java Script são completamente diferentes e possuem propósitos diversos.</p>
            </div>

        </div>


            <article className={styles.conteudos}>
                
                    <h4>Função:</h4>
                        <p className={styles.paragrafo}>Desenvolvedor com HTML,CSS JavaScript,Optimização de WebSites e
                    Interfaces melhorar Usabilidade</p>
        
                    <h4>Formação:</h4>
                        <p className={styles.paragrafo}>Licenciatura em Engenharia,Informatica ou de Sistemas.Bootcamps,Cursos online e
                    Autodidata</p>
        
                    <h4>Qualificação:</h4>
                        <p className={styles.paragrafo}>HTML,CSS,JavaScript,FrameWorks Bootstrap,Angular,ReactJS,VueJS,NextJs,Media
                    Squerie,Desing Responsivo</p>
            </article>
        <h1 className={styles.frametexto}>FRAMEWORKS</h1>

        <div className={styles.frame}>
        <img src="pngwing.com (4).png" height="200px" alt=""></img>
        </div>

        <div className={styles.accordion_div}>

        <button onClick={() => {setAccordionOne(!accordionOne)}} className={styles.accordion}>O que é framework?</button>
        <div className={`${ styles.panel  } ${accordionOne ? styles.show:styles.none}` }>
        <p className={styles.paragrafo2}>Framework é uma definição que vai além do mercado de software. Em outros contextos, refere-se a uma série de
        ações e estratégias que visam solucionar um problema bem específico. Assim, quando se deparam com esse
        cenário, os profissionais recorrem a um conjunto pronto de abordagens e otimizam os seus resultados.

        Na área de tecnologia, a definição é semelhante, mas de acordo com os aspectos técnicos de programação de
        sistemas. Trata-se de uma série de bibliotecas e classNamees — ou seja, códigos prontos — que oferecem alguma
        funcionalidade específica. Em outras palavras, é um padrão que pode ser incorporado a sistemas para agilizar
        a codificação de certas partes.

        Simplificando, é como se fossem peças prontas que podem ser inseridas em um carro. Essas peças apresentam
        uma função específica e só funcionam dentro do contexto inteiro, por isso ajudam quando o motorista precisa
        economizar o dinheiro do conserto de alguma peça defeituosa.</p>
        </div>

        <button onClick={() => {setAccordionTwo(!accordionTwo)}} className={styles.accordion}>Vantagens de utilizar framework</button>
        <div className={`${ styles.panel  } ${accordionTwo ? styles.show:styles.none}` }>
        <p className={styles.paragrafo2}>Redução de tempo
        Uma das principais vantagens é a agilidade e economia do tempo. Afinal, essas ferramentas facilitam o
        desenvolvimento dos sistemas, permitindo que os programadores não percam tempo com funcionalidades mais
        básicas.

        Desse modo, eles podem investir recursos nas funções e requisitos específicos do sistema, que o diferencia
        de outros já existentes. Assim, a produtividade aumenta consideravelmente, já que há menos desperdício de
        tempo e esforço. Isso, no geral, contribui para resultados melhores e maior satisfação dos clientes. </p>
        </div>

        <button onClick={() => {setAccordionThree(!accordionThree)}} className={styles.accordion}>Desvantagens de utilizar framework</button>
        <div className={`${ styles.panel  } ${accordionThree ? styles.show:styles.none}` }>
        <p className={styles.paragrafo2}>Problemas de configuração
        Do outro lado da análise, percebemos que os frameworks podem ser complexos para configurar em alguns casos.
        Por isso, é mais difícil adaptá-lo ao que já está implementado e garantir a comunicação deles com outras
        partes do sistema que está sendo criado.

        Além disso, há um grande consumo de tempo para instalações e definições antes mesmo do desenvolvedor
        conseguir usar os códigos.

        Dependência
        Outro problema do uso exagerado de funcionalidades prontas é a dependência. Usar muitos deles é como
        construir um sistema com partes distintas criadas e modificadas externamente.

        Assim, caso o framework sofra algum problema, o sistema no qual ele foi utilizado também é afetado. É como
        ter uma parte danificada em um carro, por exemplo: o veículo inteiro acaba sofrendo por isso.

        Códigos desnecessários
        Existem frameworks mais pesados que buscam resolver problemas maiores. O ideal é ser uma ferramenta mais
        completa e otimizar o dia a dia do desenvolvedor.

        Contudo, eles podem ser uma dor de cabeça para instalar, como já comentamos, e às vezes trazem mais códigos
        e especificações do que o necessário. Desse modo, o código fica cheio de configurações que não são
        importantes no contexto.</p>
        </div>

        <button onClick={() => {setAccordionFour(!accordionFour)}} className={styles.accordion}>Frameworks mais utilizadas no Front</button>
        <div className={`${ styles.panel  } ${accordionFour ? styles.show:styles.none}` }>
        <p className={styles.paragrafo2}>Angular - React.JS - Vue.js - Next.js- Svelte - Bootstrap.</p> 
        </div>
</div>

<section className={styles.section}>
      
      
      <h6 className={styles.h6}>POSTE SEU ARTIGO AQUI.</h6>

      <form action="" className={styles.form}>
        <div className={styles.artigo}>
          <label
            htmlFor="NameArtigo"
            className={styles.label}
          >
            Nome do artigo
          </label>
          <input
            type="text"
            name='NameArtigo'
            id='NameArtigo'
            className={styles.input}
            value={artigoName}
            onChange={({ target }) => setArtigoName(target.value)}
          />
        </div>
        <div className={styles.artigo}>
          <label htmlFor="conteudo" className={styles.label}>Descreva o artigo</label>
          <textarea
            name="conteudo"
            id="conteudo"
            cols="30"
            rows="10"
            className={styles.textArea}
            value={artigo}
            onChange={({ target }) => setArtigo(target.value)}
          />
        </div>
        <button
          className={styles.button}
          onClick={(event) => {
            event.preventDefault()
            setArtigos([...artigos, { artigoName, artigo }])
          }}
        >
          Postar
        </button>
      </form>
      <article className={styles.article}>
        {artigos && artigos.map((item, index) => {
          return (
            <div key={index} className={styles.artigoContainer}>
              <div className={styles.artigoContent}>
                <h2>{item.artigoName}</h2>
                <p>{item.artigo}</p>
              </div>
              <div className={styles.lixeira}>
                <button
                  onClick={() => {
                    const newArray = [...artigos]
                    newArray.splice(index, 1)
                    setArtigos(newArray)
                  }}
                >
                  <img src="lixeira.svg" alt="Lixeira" />
                </button>
              </div>
            </div>
          )
        })}
      </article>
    </section>
        <div className={styles.footer}>
            <footer>
                <p className={styles.pfooter}>Por Leandro Costa - 2021</p>
            </footer>
        </div> 
    </div>
    )
}
export default Home;